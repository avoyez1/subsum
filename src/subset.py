from src.dataset import Dataset


def build_subset(df_clients, agg_size, method, method_params=None):
    """
    Get a subset of clients of size agg_size with the given method (with optional parameters).
    """
    if method[:3] == "rng":
        return df_clients.get_random_subset(agg_size, method_params)

    if method[:5] == "first":
        assert df_clients.clients_size() >= agg_size
        clients = df_clients.clients()[:agg_size]
        return df_clients.get_subset_array(clients)

    if method[:3] == "all":
        return df_clients

    print("Subset generation error :", method, str(method_params))


if __name__ == "__main__":
    df_clients = Dataset.from_csv("./csv/daily_base.csv")

    print(build_subset(df_clients, 5, "rng").df.columns)

    print(build_subset(df_clients, 5, "rng", 0).df.columns)

    print(build_subset(df_clients, 5, "first-5").df.columns)